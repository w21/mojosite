# This is the installation procedure common to all the installation
# Makefiles, to be included there. The including Makefile must
# define the targets _before-copy and _after-copy, which are
# executed before and after copying the files, respectively. In the
# simplest case they may be empty, of course. Targets with names
# beginning with an underscore are not meant to be used when calling
# the Makefile by hand.

TARGET=live
INSTANCE = $$HOME/webshites/moni
EXCLUDE = --exclude=log/* --exclude=\*~ --delete-excluded
INSTALLMARKER = install-marker

_default:
	@echo
	@echo This Makefile has no meaningful default target.
	@echo

install: _say-before-do _before-copy _copy-files _after-copy _touch-marker


restart:
	@echo restart daemon with $(INSTANCE)/START.sh
        cd $(INSTANCE) && ./START.sh

_say-before-do:
	@echo TARGET is $(TARGET), INSTANCE is $(INSTANCE)

_copy-files:
	rsync -aH --delete $(EXCLUDE) . $(INSTANCE)/

_touch-marker:
	touch $(INSTANCE)/$(INSTALLMARKER)
