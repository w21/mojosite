#!/bin/sh

INSTANCE=devel
SCRIPT=moni

cd $(dirname $0)
echo starting $(basename $0) in $PWD
. ./Perlenv.sh

case $INSTANCE in
    devel)
        SERVER="morbo -v"
	URL=http://127.0.0.1:3000
        # first kill off any other running morbo; this may take a moment
        while pkill -f $URL; do sleep 1; done
        ;;
    test|live)
        SERVER=hypnotoad
        ;;
esac

exec $SERVER script/$SCRIPT
