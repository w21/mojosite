# Makefile to install a Mojolicious application from devel to the
# live environment; the specific name of the environment
# is in $(TARGET) (this is a bit stripped down from the usual)

TARGET=live
HOST=ni@holt.w21.org
STAGING = $(HOST):/var/www/moni-staging
LIVEDIR = /var/www/mn.w21.org
LIVEENV = $(HOST):$(LIVEDIR)
EXCLUDE = --exclude=log/* --exclude=\*~ --delete-excluded
INSTALLMARKER = install-marker

stage:
	rsync --delete -vaH * $(STAGING)

_default:
	@echo
	@echo This Makefile has no meaningful default target.
	@echo

install: _say-before-do _before-copy _copy-files _after-copy _touch-marker restart


restart:
	@echo restart daemon with $(LIVEDIR)/START.sh
	ssh $(HOST) "cd $(LIVEDIR) && ./START.sh"

_say-before-do:
	@echo TARGET is $(TARGET), LIVEENV is $(LIVEENV)

_copy-files:
	rsync -aH --delete $(EXCLUDE) . $(LIVEENV)/

_touch-marker:
	ssh $(HOST) touch $(LIVEDIR)/$(INSTALLMARKER)

_before-copy:

_after-copy: _patch-scripts

# all related devel/test/live differences are factored out into
# shell scripts on the top level -- I can dream, can't I?

_patch-scripts:
	ssh $(HOST) "cd $(LIVEDIR) && sed -i -e 's/INSTANCE=.*/INSTANCE=$(TARGET)/' *.sh"
