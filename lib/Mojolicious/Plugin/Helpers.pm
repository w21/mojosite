package Mojolicious::Plugin::Helpers;
use Mojo::Base 'Mojolicious::Plugin';

our $VERSION = '0.01';

my $logger;                             # need to get this only once


# register functions of this module with the Mojowork
sub register {
        my ($self, $app, $config) = @_;

        $logger = $app->log;
        $app->helper('url_prefix' => \&url_prefix);
        $app->helper(current_template_name => \&current_template_name);
        $app->helper(current_year => \&current_year);
        $app->helper(weinpic => \&weinpic);
        $logger->debug("$self ready");
}

sub weinpic {
        my ($c, $year, $fname) = @_;
        return $c->url_for("/wein/img/$year/$fname");
}

sub current_year {
        return 1900 + (localtime(time()))[5];
}

# return the name of the template currently being rendered (hopefully), as per
# Allan Cochrane <allan.cochrane@gmail.com>; this may cease to work any time
# AIUI
sub current_template_name {
        my ($c) = @_;
        my $renderer = $c->app->renderer;
        return $renderer->template_for($c);
}

sub url_prefix {
        my ($c) = @_;
        return $c->app->config('url_prefix')->{$c->app->mode()};
}

1;
# [EOB]
