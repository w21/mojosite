package Mojolicious::Plugin::PWLocal;
use Mojo::Base 'Mojolicious::Plugin';

our $VERSION = '0.01';

use Digest::MD5 qw(md5_base64);

my $logger;                             # need to get this only once


# register functions of this module with the Mojowork
sub register {
        my ($self, $app, $config) = @_;

        $logger = $app->log;
        $app->helper('pwlocal_newpass' => \&newpass);
        $app->helper('pwlocal_auth' => \&auth);
        $app->helper('pwlocal_displayname' => \&displayname);
        $logger->debug("$self ready");
}

# return displayname for username or an empty string
sub displayname {
        my ($c, $username) = @_;
        my $entry = read_from_passfile($c, $username);
        return $entry->[2] if $entry;
        return "";          
}

# return true iff user is correctly authenticated with password
sub auth {
        my ($c, $username, $password) = @_;
        my $entry = read_from_passfile($c, $username);
        #$c->app->log->debug("entry for $username is: >>$entry<<");
        return "" unless $entry;

        my ($user, $passhash, $displayname) = @$entry;
        #$c->app->log->debug("pw entry $user:$passhash:$displayname");

        # extract passhash parts: algorithm, salt, hash
        my (undef, $p_alg, $p_salt, $p_hash) = split(/\$/, $passhash);
        $c->app->log->debug("pw $user $p_alg:$p_salt:$p_hash");

        my $hashed_pw = hashpw($password, $p_salt);
        #$c->app->log->debug("hashed pw is $hashed_pw ~ :$p_hash:");

        return 1 if $hashed_pw eq $p_hash;
        return "";
}


# read local password file and return [[$user, $passhash, $displayname], ...]
# if username is specified, return entry for this user only (or undef)
sub read_from_passfile {
        my ($c, $username) = @_;
        my $one_user = defined($username); # <-- want one user entry only
        my $fname = $c->config("auth:pwlocal_file");
        unless (open(IN, "<", $fname)) {
                $c->app->log->error("PasswordHelper:: cannot open $fname: $!");
                return undef;
        }
        my $result = [];
        while (<IN>) {
                #$c->app->log->debug("read line >>$_<<");
                s/^\s*(#.*)$//;
                next if /^$/;
                chomp();
                my ($e_user, $passhash, $displayname) = split(/:/);
                #$c->app->log->debug("pw entry $e_user:$passhash:$displayname");

                my $entry = [$e_user, $passhash, $displayname];
                if ($one_user) {
                        if ($e_user eq $username) {
                                return $entry;
                        } else {
                                next;
                        }
                }
                push(@$result, $entry);
        }
        close(IN);
        return $one_user ? "" : $result;
}

# write entries (like read_from_passfile returned them) to the local password
# file
sub write_passfile {
        my ($c, $entries) = @_;
        my $fname = $c->config("auth:pwlocal_file") . ".new";
        unless (open(OUT, ">", $fname)) {
                $c->app->log->error("PasswordHelper:: cannot open >$fname: $!");
                return undef;
        }
        for my $entry (@$entries) {
                my ($user, $passhash, $displayname) = @$entry;
                say OUT ("$user:$passhash:$displayname");
        }
        unless (close(OUT)) {
                $c->app->log->error("PasswordHelper:: cannot close $fname: $!");
                return undef;
        }
        unless (rename($fname, $c->config("auth:pwlocal_file"))) {
                $c->app->log->error("PasswordHelper:: cannot move $fname "
                                    ."into place: $!");
                return undef;
        }
        $logger->debug("wrote and renamed $fname");
}

# user has a new password; register this in the local password file
sub newpass {
        my ($c, $username, $password) = @_;
        my $entries = read_from_passfile($c);
        for my $entry (@$entries) {
                my ($user) = @$entry;
                next if $user ne $username;
                $entry->[1] = md5crypt($password);
                $logger->debug("changed local pwd: $user:$entry->[1]");
                last;
        }
        write_passfile($c, $entries);
}

# return md5 hash of password for given salt
sub hashpw {
        my ($passwd, $salt) = @_;
        my $hash = md5_base64($salt . $passwd);
        #$logger->debug("$hash = md5_base64($salt . $passwd);");
        return $hash;
}

# create a new MD5 crypt string from a password using a new salt
sub md5crypt {
        my ($passwd) = @_;
        my $salt = substr(md5_base64(rand(2 ** 31 - 1)), 4, 8);
        return '$1$' . $salt . '$' . hashpw($passwd, $salt);
}


1;
# [EOB]
