package Mojolicious::Plugin::W21Auth;
use Mojo::Base 'Mojolicious::Plugin';

use W21::Auth;
our $VERSION = '0.01';
my $logger;                             # need to get this only once


# register functions of this module with the Mojowork
sub register {
        my ($self, $app, $config) = @_;

        $logger = $app->log;
        $app->helper('w21_newpass' => \&newpass);
        $app->helper('w21_auth' => \&auth);
        $app->helper('w21_displayname' => \&displayname);
        $logger->debug("$self ready");
}

# return displayname for username or an empty string
sub displayname {
        my ($c, $username) = @_;
        my $entry = getpwnam($username);
        return $entry->[5] if $entry;
        return "";          
}

# return true iff user is correctly authenticated with password
sub auth {
        my ($c, $username, $password) = @_;
        return authenticate($username, $password);
}


# user has a new password; register this in the local password file
sub newpass {
        my ($c, $username, $password, $newpassword) = @_;
        return change_password($username, $password, $newpassword);
}

1;
# [EOB]
