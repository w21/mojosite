package Moni;
use Mojo::Base 'Mojolicious';

use W21::Utils;

my $webshites_path = '/home/ni/webshites';
my $secret1 = get_random64(31);

# This method will run once at server start
sub startup {
        my $self = shift;
        $self->log->format
          (sub {
                   my (undef, $loglevel, @content) = @_;
                   return isotime_ms()." $loglevel:[$$] @content\n";
           });
        
        $self->plugin('Config');
        $self->plugin('W21Auth');
        $self->plugin('Helpers');
        W21::Utils::set_logger($self->log());

        my $url_prefix = $self->config('url_prefix')->{$self->mode()};
        $self->hook(before_dispatch => sub {
                my $c = shift;
                $c->req->url->base->path($url_prefix);
        });
        $self->secrets([$secret1]);

        # Router
        my $r = $self->routes;

        # the wine list
        $r->get('/wein/')->to('Wein#directory');
        $r->get('/wein/search/*query')->to('Wein#search');
        $r->get('/wein/search/')->to('Wein#search');
        $r->post('/wein/search')->to('Wein#search_post');
        $r->get('/wein/:year/')->to('Wein#year');
        $r->get('/wein/:year/:id')->to('Wein#bottle');

        # Authenticated routes
        my $ar = $r->under('/sites')->to('Auth#check_auth');
        $ar->get('/logout')->to('Auth#logout');

        my $wr = $r->under('/webshites')->to('Auth#check_auth');
        $wr->get('/*path')->to('Browser#path', path => '',
                               base => $webshites_path);

        # Normal route to controller
        $r->get('/')->to('example#welcome');
}

1;
