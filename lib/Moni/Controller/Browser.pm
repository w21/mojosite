package Moni::Controller::Browser;
use Mojo::Base 'Mojolicious::Controller';

use utf8;
use File::MimeInfo;
use Data::Dumper;

my $lsl = "/bin/ls -l";
my $cat = "/bin/cat";
my $log;
my $linkbase = "/webshites/";

sub trim_slash {
        my ($string) = @_;
        $string =~ s{//}{/}g;
        $string =~ s{/$}{};
        return $string;
}

# $wr->get('/*path')->to('Browser#path', base => $webshites_path);
sub path {
        my $c = shift();
        $log //= $c->app->log();
        my $path = $c->stash('path');
        $path =~ s{/$}{};
        $path =~ s{^/}{};
        $log->debug("We are at path = $path");
        # the actual pathname in the file system:
        my $realpath = $c->stash('base').'/'. $path;
        
        $c->stash(linkbase => trim_slash($c->url_prefix() . $linkbase . $path));
        $log->debug(Dumper($c->stash));

        my $content;
        if (-d $realpath) {
                my @entries = do_dir($realpath);
                s/ /&nbsp;/g for @entries;
                $c->stash(content => \@entries);
        } elsif (-f $realpath) {
                # $c->reply->static($realpath);
                $c->reply->asset(Mojo::Asset::File->new(path => $realpath));
        } else {
                $content = 'sumpfin els';
        }
}

use POSIX;
use Fcntl ':mode';

my %memo = ();                          # memoize results

# memoize function call results (for essentially string-valued @_ only)
sub memoize {
        my ($funcref, @args) = @_;
        return $memo{$funcref}->{"@args"}
          // ($memo{$funcref}->{"@args"} = $funcref->(@args));
}

sub getuname { return getpwuid($_[0]) || $_[0]; }
sub getgname { return getgrgid($_[0]) || $_[0]; }

# return a single-character file type marker as used with ls
sub filetype {
    my ($mode) = @_;

    S_ISREG($mode)  && return "-";
    S_ISDIR($mode)  && return "d";
    S_ISLNK($mode)  && return "l";
    S_ISFIFO($mode) && return "p";
    S_ISSOCK($mode) && return "s";
    #S_ISDOOR($mode) && return "D";    # Solaris only
    S_ISCHR($mode)  && return "c";
    S_ISBLK($mode)  && return "b";
    return "?";                        # Dunno.
}

my %msym = (0 => '---', 1 => '--x', 2 => '-w-', 3 => '-wx', 
            4 => 'r--', 5 => 'r-x', 6 => 'rw-', 7 => 'rwx', );

sub mode_s {
        my ($m) = @_;
        my $mode_s = "";
        my $tribble = 2;
        for my $mask (0700, 0070, 0007) {
                $mode_s .= $msym{($m & $mask) >> ($tribble * 3)};
                $tribble--;
        }
        substr($mode_s, 2, 1) = $m & 0100 ? 's' : 'S' if $m & 04000;
        substr($mode_s, 5, 1) = $m & 0010 ? 's' : 'S' if $m & 02000;
        substr($mode_s, 8, 1) = $m & 0001 ? 't' : 'T' if $m & 01000;
        return $mode_s;
}


# construct the line for a single file
sub do_entry {
        my ($fname, @fstat) = @_;
        my ($mode, $lcount, $uid, $gid, $size, $amtime) =
          @fstat[2,3,4,5,7,9];
        return [ filetype($mode) . mode_s($mode & 07777),
                 $lcount,
                 memoize(\&getuname, $uid),
                 memoize(\&getgname, $gid),
                 $size,
                 strftime("%Y-%m-%d %H:%M:%S", localtime($amtime)),
                 $fname,
            ];
}

# iterate through a directory
sub do_dir {
        my ($dir) = @_;
    
        unless(opendir DIR, $dir) {
                warn("$0: cannot open directory $dir: $!\n");
                return;
        }
        my @entries = readdir(DIR);
        closedir(DIR);
        my @lines = ();
        for my $entry (sort(@entries)) {
                my @fstat = lstat("$dir/$entry");
                unless (@fstat) {
                        warn("$0: cannot stat $entry ($!)\n");
                        next;
                }
                push(@lines, do_entry($entry, @fstat));
        }
        return @lines;
}

1;
# EOF
