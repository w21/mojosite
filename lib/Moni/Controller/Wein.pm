package Moni::Controller::Wein;
use Mojo::Base 'Mojolicious::Controller';
use Mojolicious;

use Unicode::Normalize;
use Encode qw(decode encode);
use Data::Dumper;
use W21::Utils;
use Digest::MD5 qw(md5_hex);

# read in the captions for the year -> [fname, title, description, id]
# keys are fnames *and* the id
sub readin_year {
        my ($c, $year) = @_;
        unless ($year =~ /^[0-9]{4}$/) {
                $c->stash(status => 404);
                return undef;
        }
        my $fh;
        my $captions =
          sprintf("%s/%s/captions.txt", $c->config('wein_path'), $year);
        unless (open($fh, '<', $captions)) {
                $c->stash(status => 404);
                return undef;
        }
        my $result = {};
        while (<$fh>) {
                chomp();
                my ($pic, $title, $description) = split(/\t/, $_, 3);
                my $id = md5_hex($title);
                $title = NFC(decode('UTF-8', $title));
                $description = NFC(decode('UTF-8', $description));
                # debug("title = $title");
                my $entry = [$pic, $title, $description, $id];
                $result->{$pic} = $entry; # still used for sorting
                $result->{$id} //= [];
                push(@{$result->{$id}}, $entry);
        }
        close($fh);
        return $result;
}

# return true if the entry somehow matches all query fields
sub match_all {
        my ($text, @query) = @_;
        for my $term (@query) {
                if (lc($text) !~ /$term/i) {
                        return '';
                }
        }
        return 1;
}

sub readin_all_years {
        my ($c) = @_;
        my %entries = ();
        for my $year ($c->config('wein_first_year')..$c->current_year()) {
                debug("read in $year");
                my $y_entries = $c->readin_year($year);
                my %ys = %$y_entries;
                my @keys = keys(%$y_entries);
                @entries{@keys} = @ys{@keys};
        }
        return \%entries;
}

# $r->get('/wein/search/*query')->to('Wein#search');
sub search {
        my ($c) = @_;
        my $query = trim($c->param('query') // '');
        my $reverse = $c->param('reverse') // 1;
        my @terms = split(' ', $query);
        my $result = { title => [], text => [] };
        if (@terms) {
                my $entries = $c->readin_all_years() or return 0;
                for my $key (sort(grep(/^\d{4}-\d\d-\d\d_/, keys(%$entries)))) {
                        my $entry = $entries->{$key};
                        if (match_all($entry->[1], @terms)) {
                                push(@{$result->{title}}, $entry);
                        } elsif (match_all("$entry->[0] $entry->[2]", @terms)) {
                                push(@{$result->{text}}, $entry);
                        }
                }
        }
        $c->stash(query => $query);
        $c->stash(result => $result);
        $c->stash(reverse => $reverse);
}

# $r->post('/wein/search')->to('Wein#search_post');
sub search_post {
        my ($c) = @_;
        my $query = $c->param('query');
        $c->redirect_to($c->url_for("/wein/search/$query"));
}

sub directory {
        my ($c) = @_;
        my $reverse = $c->param('reverse') // 0;
        $c->stash(result =>
                  [$c->config('wein_first_year')..$c->current_year()]);
        $c->stash(reverse => $reverse);
}

# $r->get('/wein/:year')->to('Wein#year');
sub year {
        my ($c) = @_;
        my $year = $c->param('year');
        my $reverse = $c->param('reverse') // 0;
        my $entries = $c->readin_year($year) or return 0;
        $c->stash(result => $entries);
        $c->stash(year => $year);
        $c->stash(reverse => $reverse);
}

# $r->get('/wein/:year/:id')->to('Wein#bottle');
sub bottle {
        my ($c) = @_;
        debug("in Wein#bottle");
        my $year = $c->param('year');
        my $entries = $c->readin_year($year) or return 0;
        my $id = $c->param('id');
        debug("id is $id");
        my $entry = $entries->{$id};
        # debug("entry:", Dumper($entry));
        # $entry = '';
        unless ($entry) {
                $c->stash(status => 404);
                return undef;
        }
        $c->stash(id => $id);
        $c->stash(year => $year);
        $c->stash(result => $entry);
        # debug("result:", Dumper($c->stash('result')));
}

1;
