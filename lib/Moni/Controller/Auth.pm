package Moni::Controller::Auth;
use Mojo::Base 'Mojolicious::Controller';

use utf8;
use Data::Dumper;

sub check_auth {
        my $c = shift();
        # $c->app->log->debug(Dumper($c->req));

        return 1 if is_logged_in($c);

        # get basic authentication credentials
        my $creds = $c->req->url->to_abs->userinfo || '';
        if ($creds && !$c->flash('logout')) {
                $c->app->log->debug("creds: $creds");
                my ($basic_user, $basic_pass) = split(/:/, $creds);
                if ($c->pwlocal_auth($basic_user, $basic_pass)) {
                        $c->session(username => $basic_user); 
                        return 1;
                } else {
                        sleep(1);
                }
        }

        # no valid authentication method found, so bounce back a 401
        my $realm = $c->config("auth:basicauth_realm")
          || "Hello stranger";
        $c->res->headers->www_authenticate("Basic realm=$realm");
        $c->rendered(401);
        return 0;
}


sub is_logged_in {
        my $c = shift();

        return 1 if $c->session('username');
        # $c->redirect_to('/login');
        return 0;
}

# present the login form
sub login_form {
        my $c = shift;
        $c->stash(error_message => $c->flash('error_message') // '');
        $c->stash(success_message => $c->flash('success_message') // '');
        $c->render();
}

# login for submit action
sub login_submit {
        my $c = shift;
        my $username = $c->param('j_username');
        my $password = $c->param('j_password');
        $c->app->log->debug("have $username:$password");

        if ($c->pwlocal_auth($username, $password)) {
                $c->session(displayname => $c->pwlocal_displayname($username));
                $c->session(username => $username);
                $c->redirect_to('/service');
                return;
        }
        sleep(1);
        $c->session(expires => 1);
        $c->flash(error_message => 'Benutzername oder Passwort ungÃŒltig');
        $c->redirect_to('/login');
}

sub logout {
        my $c = shift();
        $c->flash(logout => 1);
        $c->redirect_to('/');
}

1;
