package Moni::Controller::Diverse;
use Mojo::Base 'Mojolicious::Controller';
use Mojolicious;

# This action will render a template
sub about {
        my $c = shift();

        $c->stash(username => $c->session('username') // "stranger");
        my $version = $Mojolicious::VERSION;
        $c->stash(mojo_version => $version);

        # Render template "example/welcome.html.ep" with message
        $c->render();
}

my %is_valid_api_version = (v01 => 1,
                         );
# check format and API version
sub check_api_version {
        my $c = shift();
        my $version = $c->param('api_version');
        unless ($is_valid_api_version{$version}) {
                $c->stash(status => 406);
                $c->render(text => 'unsupported API version');
                return 0;
        }
        return 1;
}

sub index_page {
        my $c = shift();

        $c->render();
}

1;
