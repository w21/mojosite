# to be sourced for the right perl/mojo install location

export PERL_LOCAL_LIB_ROOT=$HOME/sw/mojo
export PERL5LIB=$PERL_LOCAL_LIB_ROOT/lib/perl5:/opt/w21/lib/perl5
export PERL_MB_OPT="--install_base $PERL_LOCAL_LIB_ROOT"
export PERL_MM_OPT=INSTALL_BASE=$PERL_LOCAL_LIB_ROOT
PATH=$PERL_LOCAL_LIB_ROOT/bin:$PATH

